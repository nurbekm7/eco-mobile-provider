import sbt._

object Version {
  val akka            = "2.4.1"
  val spray           = "1.3.3"
  val spray_json      = "1.3.2"
  val scala           = "2.11.7"
  val scalaTest       = "2.2.4"
  val specs2          = "3.3.1"
  val specs2Core      = "3.8.5"
  val specs2Mock      = "3.8.6-20161107094644-7ddfac2"
  val typesafeConfig  = "1.3.0"
  val logback         = "1.1.3"
  val jodaTime        = "2.8.2"
  val jodaConverter   = "1.7"
  val emailValidator  = "0.4.0"
  val amqpClient      = "3.5.5"
  val stoneAmqpClient = "1.5"
  val mockito         = "1.10.19"
  val jwt             = "0.4.1"
  val ecoDomain       = "1.0.0"
  val ecoExceptions   = "1.0.0"
  val ecoMessages     = "1.0.0"
  val epsDomain       = "1.0.0"
  val aerospike       = "3.1.8"
}

object Library {
  val akkaActor       = "com.typesafe.akka"    %% "akka-actor"        % Version.akka
  val akkaSlf4j       = "com.typesafe.akka"    %% "akka-slf4j"        % Version.akka
  val akkaPersist     = "com.typesafe.akka"    %% "akka-persistence"  % Version.akka
  val sprayCan        = "io.spray"             %% "spray-can"         % Version.spray
  val sprayRouting    = "io.spray"             %% "spray-routing"     % Version.spray
  val sprayHttp       = "io.spray"             %% "spray-http"        % Version.spray
  val sprayHttpx      = "io.spray"             %% "spray-httpx"       % Version.spray
  val sprayClient     = "io.spray"             %% "spray-client"      % Version.spray
  val sprayUtil       = "io.spray"             %% "spray-util"        % Version.spray
  val sprayShapeless  = "io.spray"             %% "spray-routing-shapeless2" % Version.spray
  val sprayJson       = "io.spray"             %% "spray-json"        % Version.spray_json

  val typesafeConfig  = "com.typesafe"         %  "config"            % Version.typesafeConfig

  val akkaTestKit     = "com.typesafe.akka"    %% "akka-testkit"      % Version.akka  % "test"
  val sprayTestKit    = "io.spray"             %% "spray-testkit"     % Version.spray % "test"
  val specs2Core      = "org.specs2"           %% "specs2-core"       % Version.specs2Core % "test"
  val spec2           = "org.specs2"           %% "specs2"            % Version.specs2 % "test"
  val specs2Mock      = "org.specs2"           %% "specs2-mock"       % Version.specs2Mock % "test"
  val scalaTest       = "org.scalatest"        %  "scalatest_2.11"    % Version.scalaTest % "test"

  val logback         = "ch.qos.logback"       %  "logback-classic"   % Version.logback
  val joda            = "joda-time"            %  "joda-time"         % Version.jodaTime
  val jodaConverter   = "org.joda"             %  "joda-convert"      % Version.jodaConverter

  val emailValidator  = "uk.gov.hmrc"          %% "emailaddress"      % Version.emailValidator

  val amqpClient      = "com.rabbitmq"         % "amqp-client"        % Version.amqpClient
  val stoneAmqpClient = "com.github.sstone"    % "amqp-client_2.11"   % Version.stoneAmqpClient
  val mockito         = "org.mockito"          % "mockito-core"       % Version.mockito
  val jwt             = "com.jason-goodwin"    %% "authentikat-jwt"   % Version.jwt
  val ecoDomain       = "kz.dar.eco"           %% "dar-eco-domain"    % Version.ecoDomain
  val ecoExceptions   = "kz.dar.eco"           %% "dar-eco-exceptions"  % Version.ecoExceptions
  val ecoMessages     = "kz.dar.eco"           %% "dar-eco-messages"  % Version.ecoMessages
  val epsDomain       = "kz.dar.eco"           %% "eps-domain-model"  % Version.epsDomain
  val aerospike       = "com.aerospike"        % "aerospike-client"   % Version.aerospike

  val levelDB         = "org.iq80.leveldb"            % "leveldb"          % "0.7"
  val levelDBjni      = "org.fusesource.leveldbjni"   % "leveldbjni-all"   % "1.8"

}

object Dependencies {

  import Library._

  val depends = Seq(
    akkaActor,
    akkaSlf4j,
    sprayCan,
    sprayHttp,
    sprayHttpx,
    sprayClient,
    sprayUtil,
    sprayShapeless,
    sprayJson,
    specs2Core,
    specs2Mock,
    typesafeConfig,
    akkaTestKit,
    sprayTestKit,
    specs2Core,
    scalaTest,
    logback,
    joda,
    jodaConverter,
    amqpClient,
    stoneAmqpClient,
    mockito,
    jwt,
    ecoDomain,
    ecoExceptions,
    ecoMessages,
    epsDomain,
    aerospike,
    akkaPersist,
    levelDB,
    levelDBjni,
    "org.scala-lang.modules" %% "scala-xml" % "1.0.5",
    "org.scala-lang.modules" % "scala-parser-combinators_2.11" % "1.0.4",
    "org.scala-lang" % "scala-reflect" % "2.11.7",
    "org.scala-lang" % "scala-compiler" % "2.11.7",
    "org.scalaz" %% "scalaz-core" % "7.1.7"
  )
}
