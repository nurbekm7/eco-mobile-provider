
organization := "kz.dar.eco"

version := "1.0.0"

name := "eco-mobile-provider"

scalaVersion := "2.11.8"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"
resolvers += Resolver.bintrayRepo("hmrc", "releases")
resolvers += "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
resolvers += "Artifactory" at "http://54.229.103.37/artifactory/sbt-local/"

scalacOptions in Test ++= Seq("-Yrangepos")
libraryDependencies ++= Dependencies.depends

enablePlugins(JavaAppPackaging)

Revolver.settings

