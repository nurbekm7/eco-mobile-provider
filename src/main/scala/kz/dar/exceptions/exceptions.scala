package kz.dar.exceptions

import kz.dar.eco.common.exception.{DarErrorSystem, ErrorCode, ErrorSeries, ErrorSystem}

/**
  * Created by nurbek on 2/3/17.
  */

/**
  * API series
  */
object ApiErrorSeries {

  case object IDENTIFICATION_REPO extends ErrorSeries {
    override val series = 31
  }

  case object IDENTIFICATION_API extends ErrorSeries {
    override val series = 32
  }

}

/**
  * Api error codes
  */
object ApiErrorCodes {

  case object INTERNAL_SERVER_ERROR extends ErrorCode {
    override val system: ErrorSystem = DarErrorSystem
    override val series = ApiErrorSeries.IDENTIFICATION_REPO
    override val code = 1
  }

  case object JSON_PARSE_ERROR extends ErrorCode {
    override val system: ErrorSystem = DarErrorSystem
    override val series = ApiErrorSeries.IDENTIFICATION_REPO
    override val code = 3
  }

  case object COUCH_DB_ERROR extends ErrorCode {
    override val system: ErrorSystem = DarErrorSystem
    override val series = ApiErrorSeries.IDENTIFICATION_REPO
    override val code = 10
  }

  case object TIMEOUT_ERROR extends ErrorCode {
    override val system: ErrorSystem = DarErrorSystem
    override val series = ApiErrorSeries.IDENTIFICATION_REPO
    override val code = 504
  }

}