package kz.dar.exceptions

import java.util.Locale

import com.typesafe.config.{Config, ConfigException, ConfigFactory}
import kz.dar.eco.common.exception.ErrorLocaleContext
import scala.collection.JavaConversions._

/**
 * Error messages provider
 *
 * Created by Rustem on 18-06-2015.
 */

object ErrorLocaleContextFactory {

  val contexts : Seq[ErrorLocaleContext] = LocalizedMessages.locales.map{l => new ApiErrorLocaleContext(new Locale(l))}

  def getContextForLocale(locale: Locale) : Option[ErrorLocaleContext] = contexts.find(_.locale == locale)

}

/**
 * Returns error message for given error code(message key) and locale
 *
 * @param locale - message localization
 */
class ApiErrorLocaleContext(override val locale: Locale) extends ErrorLocaleContext {

  /**
   * Returns localized message for the given error code(message key)
   *
   * @param fullErrorCode - error code (message key)
   * @return localized message
   */
  def getLocalizedMessage(fullErrorCode: String) : String = {

    LocalizedMessages.getMessage(locale, fullErrorCode)

  }

}

/**
 * Loads localized messages from config using TypeSave Conf lib
 */
object LocalizedMessages {

  /**
   * Available locales
   */
  val locales : Seq[String] = ConfigFactory.load().getStringList("languages")


  /**
   * Configs for each locale
   */
  private val configs : Map[String, Config] = locales.map(locale => locale -> ConfigFactory.load("messages." + locale + ".properties")).toMap

  /**
   * Returns localized message
   *
   * @param locale  - message locale
   * @param key     - message key
   * @return localized message
   */
  def getMessage(locale: Locale, key: String): String= {

    val config : Config = configs.getOrElse(locale.getLanguage, configs.head._2)

    try {
      config.getString(key)
    } catch {
      case e: ConfigException => key
    }

  }
}