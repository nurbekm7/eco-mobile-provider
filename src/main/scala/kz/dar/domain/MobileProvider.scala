package kz.dar.domain

import kz.dar.eco.domain.core.DomainEntity

/**
  * Created by nurbek on 2/9/17.
  */
case class MobileProvider(mobile: String,
                          provider: String) extends DomainEntity