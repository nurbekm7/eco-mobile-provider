package kz.dar

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import com.aerospike.client.{AerospikeClient, Host, IAerospikeClient}
import com.typesafe.config.ConfigFactory
import kz.dar.core.MobileProviderActor
import kz.dar.repository.MobileRepo
import spray.can.Http
import kz.dar.rest._
import kz.dar.service.{MobileActor, UpdateFilesActor}



/**
  * Created by nurbek on 6/2/16.
  */
object Boot extends App   {

   implicit val system = ActorSystem("eco-mobile-provider")

  val config = ConfigFactory.load()

  def getStringConfigKey(key: String) : String = {
    try {
      sys.env(key)
    } catch {
      case e: NoSuchElementException =>
        config.getString(key)
    }
  }

  def getIntConfigKey(key: String) : Int = {
    try {
      sys.env(key).toInt
    } catch {
      case e: NoSuchElementException =>
        config.getInt(key)
    }
  }

  val hosts = getStringConfigKey("aerospike.hosts")

  val aerospikeHosts: Seq[Host] = hosts.split(",").map {
    h =>
      val p = h.split(":")
      new Host(p.head, p.last.toInt)
  }

  implicit val aerospikeClient: IAerospikeClient = new AerospikeClient(null, aerospikeHosts:_*)



  // create and start our service actor
  val mobileRepo = new MobileRepo()
  val updateFilesActor = system.actorOf(UpdateFilesActor.props(), "filesActor")
  val mobileActor = system.actorOf(MobileActor.props(), "mobileActor")


  val mobileServiceProps = (_: Unit) => MobileProviderActor.props(
    mobileRepo,
    mobileActor,
    updateFilesActor
  )


  val serviceActor = system.actorOf(Props(new RestRouting(mobileServiceProps)), name = "mobile-rest")

  system.registerOnTermination {
    system.log.info("mobile provider repository shutdown.")
  }


  val host = config.getString("application.server.host")
  val port = config.getInt("application.server.port")

  IO(Http) ! Http.Bind(serviceActor, host, port = port)

}
