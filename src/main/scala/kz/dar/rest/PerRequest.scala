package kz.dar.rest

import java.util.Locale

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{OneForOneStrategy, _}
import kz.dar.eco.common.exception.{ApiException, GatewayTimeoutErrorException, ServerErrorRequestException}
import kz.dar.eco.domain.core.{Accepted, DomainEntity, EmptyResponse, SeqEntity}
import kz.dar.exceptions.{ApiErrorCodes, ErrorLocaleContextFactory, LocalizedMessages}
import kz.dar.rest.PerRequest.{WithActorRef, WithProps}
import kz.dar.utils.{RestMessage, Serializers, Validation}
import spray.http.{HttpHeaders, StatusCode}
import spray.http.StatusCodes._
import spray.httpx.Json4sSupport
import spray.routing.RequestContext

import scala.concurrent.duration._

trait PerRequest extends Actor with ActorLogging with Json4sSupport with Serializers {

  import context._

  val json4sFormats = formats

  def r: RequestContext
  def target: ActorRef
  def message: RestMessage

  target ! message

  def receive = {
    case SeqEntity(res)         => complete(OK, res)
    case res: Seq[RestMessage]  => complete(OK, res)
    case res: RestMessage       => complete(OK, res)
    case e: EmptyResponse       => complete(NoContent, None)
    case res: Accepted          => complete(StatusCode.int2StatusCode(res.status), res)
    case res: DomainEntity      => complete(OK, res)
    case v: Validation          => complete(BadRequest, v)
    case ReceiveTimeout         => completeWithError(GatewayTimeoutErrorException(ApiErrorCodes.TIMEOUT_ERROR))
    case e: ApiException        => completeWithError(e)
  }

  def complete[T <: AnyRef](status: StatusCode, obj: T) = {
    r.complete(status, obj)
    stop(self)
  }

  /**
   * Completes request with error
   * @param apiException
   */
  def completeWithError(apiException: ApiException) = {
    val language = getLanguageFromHeader(r)
    val errorInfo = apiException.getErrorInfo(ErrorLocaleContextFactory.getContextForLocale(new Locale(language)))
    log.error(s"Application return expected error status code [${language}] ${apiException.status} with entity ${errorInfo} ")
    complete(apiException.status, errorInfo)
  }

  override val supervisorStrategy =
    OneForOneStrategy() {

      /**
       * Catching Api Exceptions
       */
      case e: ApiException => {

        completeWithError(e)

        Stop
      }

      /**
       * Catching any other exceptions
       */
      case e => {

        val error = ServerErrorRequestException(ApiErrorCodes.INTERNAL_SERVER_ERROR, Some(e.getMessage))
        completeWithError(error)

        Stop

      }
    }

  /**
    * Returns requested language from header.
    * If not presented, then default language is returned
    *
    * @param context - request context
    * @return language as string
    */
  def getLanguageFromHeader(context: RequestContext) : String = {

    val defaultLanguage = "ru"

    context.request.headers.find(_.name.equalsIgnoreCase("Accept-Language")) match {

      case Some(header) =>

        val lan : Option[String] = header match {

          case HttpHeaders.`Accept-Language`(languages) =>

            languages.find(_.primaryTag != "null").map(_.primaryTag)

          case _ =>

            LocalizedMessages.locales.find(header.value.indexOf(_) >= 0)

        }

        lan match {
          case Some(l) => l
          case _ => defaultLanguage
        }

      case _ => defaultLanguage

    }

  }

}

object PerRequest {
  case class WithActorRef(r: RequestContext, target: ActorRef, message: RestMessage) extends PerRequest

  case class WithProps(r: RequestContext, props: Props, message: RestMessage) extends PerRequest {
    lazy val target = context.actorOf(props)
  }
}

trait PerRequestCreator {
  this: Actor =>

  def perRequest(r: RequestContext, target: ActorRef, message: RestMessage) =
    context.actorOf(Props(new WithActorRef(r, target, message)))

  def perRequest(r: RequestContext, props: Props, message: RestMessage) =
    context.actorOf(Props(new WithProps(r, props, message)))
}