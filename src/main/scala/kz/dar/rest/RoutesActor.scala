package kz.dar.rest

import akka.actor.{Actor, ActorLogging, Props}
import kz.dar.core._
import spray.httpx.{Json4sSupport, SprayJsonSupport}
import spray.routing._
import spray.http._
import MediaTypes._
import kz.dar.utils._
import akka.util.Timeout
import kz.dar.service.{MobileActor, UpdateFilesActor}
import kz.dar.core.MobileProviderActor._
import kz.dar.repository.MobileRepo

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class RestRouting(mobileServiceProps: Unit => Props)  extends HttpService with Actor with PerRequestCreator
                                                            with Json4sSupport with Serializers  with ActorLogging  {

  implicit def actorRefFactory = context
  implicit val executionContext: ExecutionContext = context.dispatcher

  val json4sFormats = formats

  implicit val timeout = Timeout(5.seconds)


  def receive = runRoute(routes)

  val routes =

    path("mobile"/"type"/Segment){ (mobileNumber) =>
      get {
          handleRequest {
            GetMobileType(mobileNumber)
          }
      }
    }~
    path("uploadFromFiles"){
      post {
          handleRequest {
            UploadFromFile()
          }
        }
    }~
    path("updateFiles"){
      post {
          handleRequest {
            UpdateFiles()
          }
        }
    }
//    path("inv"/"listAll"){
//    get {
//      respondWithMediaType(`application/json`) {
//        handleRequest {
//          GetInvoices()
//        }
//      }
//    }
//  }~
//   path("deleteInv"/IntNumber) { (ID) =>
//     post {
//       handleRequest {
//         DeleteInvoice(ID)
//       }
//
//
//     }
//   }~



  def handleRequest(message : RestMessage): Route = {
    ctx => perRequest(ctx, mobileServiceProps(), message)
}

}

