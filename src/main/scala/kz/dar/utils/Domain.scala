package kz.dar.utils

import kz.dar.eco.domain.core.DomainObject
import kz.dar.eco.messages.core.MessageType

/**
  * Created by nurbek on 6/2/16.
  */

trait ApiMessage extends DomainObject{
  val messageType: MessageType
}

trait RestMessage

case class Validation(message: String)

// Exceptions
case object BagRequestException extends Exception("This is Bad Request, Man")
