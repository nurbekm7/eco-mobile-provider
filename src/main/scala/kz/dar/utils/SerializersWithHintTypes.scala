package kz.dar.utils

import kz.dar.eco.domain.serializers._
import kz.dar.eco.eps.domain.EpsSerializer
import kz.dar.eco.messages.MessageSerializer
import org.json4s._
import org.json4s.native.Serialization

/**
 * Created by Rustem on 03-12-2015.
 */
trait SerializersWithHintTypes extends DateTimeSerializer with ProfileSerializer with PersonaSerializer with UserContextSerializer
  with MessageSerializer with EpsSerializer with CoreSerializers {

  val dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val formats = Serialization.formats(
    ShortTypeHints(
      coreTypeHints ++ profileTypeHints ++ personaTypeHints ++ userContextTypeHints ++ messageHintTypes ++ epsHintTypes
    )
  ) +
    new DateTimeSerializer() ++
    profileFormats ++
    userContextFormats ++
    epsDomainFormats ++
    personaFormats addKeySerializers
    profileKeyFormats

}
