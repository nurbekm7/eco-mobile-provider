package kz.dar.utils

import kz.dar.eco.domain.serializers._
import kz.dar.eco.messages.MessageSerializer
import org.json4s.NoTypeHints
import org.json4s._
import org.json4s.native.Serialization

/**
 * Created by irybakov on 2/19/16.
 */
trait Serializers extends DateTimeSerializer with ProfileSerializer with PersonaSerializer with UserContextSerializer with HalvaSerializers with MessageSerializer {

  val dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val formats = Serialization.formats(NoTypeHints) +
    new DateTimeSerializer() ++
    profileFormats ++
    userContextFormats ++
    halvaFormats ++
    personaFormats addKeySerializers
    profileKeyFormats



}

