package kz.dar.service

import akka.actor.Status.Success
import akka.actor.{Actor, Props}
import akka.persistence.PersistentActor
import com.aerospike.client.{Bin, IAerospikeClient}
import com.aerospike.client.policy.WritePolicy
import kz.dar.core.MobileProviderActor
import kz.dar.eco.domain.core.Accepted
import kz.dar.repository.{AerospikeRepository, IMobileRepo}
import kz.dar.service.MobileActor.UpdateDB
import kz.dar.service.UpdateFilesActor._
import kz.dar.utils.{RestMessage, Validation}

/**
  * Created by nurbek on 6/8/16.
  */


object MobileActor {

  case class UpdateDB()

  def props()(implicit client:IAerospikeClient): Props =
    Props(new MobileActor()(client))

}


class MobileActor()(implicit client: IAerospikeClient) extends PersistentActor with AerospikeRepository {

  override def persistenceId: String = "mobile-upload-actor"

  override val namespace = "test"
  override val set = "mobile"

  val bufferedSource = io.Source.fromFile("./Port_All_201612260000_615.csv")
  var count = 1


  def receiveCommand: Receive = {
    case "boom"  => throw new Exception("boom")
    case "count"  => count = 1
    case UpdateDB =>

      val policy = new WritePolicy()
      for (line <- bufferedSource.getLines.drop(1)) {
        val cols = line.split(",").map(_.trim)
        persist(count) {c => count+=1}

        if(cols(0).nonEmpty){
          println(cols(0))
          val key = createKey(cols(0))
          val bins = Seq(
            new Bin("mobile", cols(0)),
            new Bin("ownerId", cols(1)),
            new Bin("route", cols(2)),
            new Bin("portDate", cols(3))
          )

          client.put(policy, key, bins: _*)
        }

      }
      bufferedSource.close

      sender ! Success()





  }

  def receiveRecover: Receive = {
    case s : Int =>  count = s
  }



}


