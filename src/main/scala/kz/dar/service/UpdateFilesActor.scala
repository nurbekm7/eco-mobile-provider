package kz.dar.service

import akka.actor.Status.Success
import akka.actor.{Actor, Props}
import kz.dar.repository.IMobileRepo
import kz.dar.service.UpdateFilesActor.{UpdateFiles}

/**
  * Created by nurbek on 2/6/17.
  */


object UpdateFilesActor {

  case class UpdateFiles()

  def props(): Props =  Props(new UpdateFilesActor)

}


class UpdateFilesActor() extends  Actor {

  def receive ={

    case UpdateFiles =>

      println("UpdateFiles")

      sender() ! Success()

  }
}