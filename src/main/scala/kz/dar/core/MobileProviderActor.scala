package kz.dar.core

import akka.actor.Status.{Failure, Success}
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import kz.dar.service.MobileActor._
import kz.dar.core.MobileProviderActor._
import kz.dar.eco.common.exception._
import kz.dar.eco.domain.core.Accepted
import kz.dar.repository.IMobileRepo
import kz.dar.service.{MobileActor, UpdateFilesActor}
import kz.dar.utils._
import kz.dar.utils.{ApiMessage, BagRequestException}
import akka.pattern.pipe
import kz.dar.domain.MobileProvider
import kz.dar.exceptions.ApiErrorCodes

import scala.concurrent.ExecutionContext.Implicits.global



/**
  * Created by nurbek on 6/2/16.
  */
object MobileProviderActor {


  case class GetMobileType(mobileNumber : String) extends RestMessage

  case class UpdateFiles() extends RestMessage

  case class UploadFromFile() extends RestMessage

  case class MobileProviderLoaded(mobileProvider: MobileProvider)



  def props(mobileRepo : IMobileRepo, mobileActor: ActorRef, updateFilesActor: ActorRef): Props =
    Props(new MobileProviderActor(
      mobileRepo, mobileActor, updateFilesActor
    ))
}

class MobileProviderActor(mobileRepo : IMobileRepo, mobileActor: ActorRef, updateFilesActor: ActorRef) extends Actor with ActorLogging {

  override def aroundReceive(receive: Actor.Receive, msg: Any): Unit = msg match {
    case ex: ApiException =>

      log.error(s"Received exception: ${ex.toString}")

      throw ex

    case ex: Exception =>

      log.error(s"Received exception: ${ex.toString}")

      throw ServerErrorRequestException(WalletErrorCodes.INTERNAL_SERVER_ERROR(WalletErrorSeries.PAYROLL_PROCESSOR), Option(ex.getMessage))

    case _ => receive.applyOrElse(msg, unhandled)
  }


  override def receive: Receive = {

         case UpdateFiles() =>
           updateFilesActor ! UpdateFilesActor.UpdateFiles
           context.become(waitingResponses)

         case UploadFromFile() =>

           mobileActor ! MobileActor.UpdateDB

           context.become(waitingResponses)

         case GetMobileType(mobileNumber) =>

           pipe {
             mobileRepo.getMobileType(mobileNumber)
           } to self

           context.become(waitingMobileProvider)

         case _ =>  throw BagRequestException



       }

  def waitingResponses: Receive = {

    case Success(_) =>

      log.info(s"Data saved successfully")

      context.parent ! Accepted()

    case Failure(ex) =>
      log.error(s"Unexpected message ${ex}")
      throw ServerErrorRequestException(ApiErrorCodes.INTERNAL_SERVER_ERROR)


  }

  def waitingMobileProvider: Receive = {

    case Some(m : MobileProvider) =>

      log.info(s"Data saved successfully")

      context.parent ! m

    case None =>

      context.parent ! NotFoundException(WalletErrorCodes.PAYROLL_ORDER_NOT_FOUND(WalletErrorSeries.PAYROLL_PROCESSOR))

      context.stop(self)

    case ex: Exception =>

      log.error(s"Received exception: ${ex.toString}")

      context.parent ! ex

      context.stop(self)

  }
}

