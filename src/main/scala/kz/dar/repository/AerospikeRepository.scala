package kz.dar.repository

import com.aerospike.client.Key
import com.aerospike.client.query.Statement

/**
  * Created by nurbek on 2/3/17.
  */
trait AerospikeRepository {

  protected val namespace: String
  protected val set: String

  protected def createStatement: Statement = {
    val statement = new Statement()
    statement.setNamespace(namespace)
    statement.setSetName(set)
    statement
  }

  protected def createKey(id: String): Key = new Key(namespace, set, id)

  protected def createKey(id: String, otherSet: String): Key = new Key(namespace, otherSet, id)

}

