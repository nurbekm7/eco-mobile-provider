package kz.dar.repository

import com.aerospike.client.{Bin, IAerospikeClient, Record, Value}
import com.aerospike.client.policy.WritePolicy
import com.aerospike.client.query.{Filter, RecordSet, Statement}
import kz.dar.domain.MobileProvider
import kz.dar.eco.eps.domain.payroll.PayrollOrder
import org.joda.time.DateTime

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


/**
  * Created by nurbek on 2/3/17.
  */
class MobileRepo()(implicit client: IAerospikeClient) extends IMobileRepo with AerospikeRepository{


  override val namespace = "test"
  override val set = "mobile_provider"


  override def getMobileType(mobileNumber: String): Future[Option[MobileProvider]] = {

    Future {

      println("Month, Income, Expenses, Profit")
      val key = createKey(mobileNumber)

      val record = client.get(null, key)

      println(record)

      recordToMobileProvider(record)

    }
  }

  def recordToMobileProvider(record: Record): Option[MobileProvider]  = {

    if(record != null) {
      Some(MobileProvider(
        mobile = record.getString("number"),
        provider = record.getString("ownerId")
      ))

    } else {
      None
    }

  }
}
