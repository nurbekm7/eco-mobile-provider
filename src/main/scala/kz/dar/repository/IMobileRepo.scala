package kz.dar.repository



import kz.dar.domain.MobileProvider

import scala.concurrent.Future
/**
  * Created by nurbek on 2/3/17.
  */
trait IMobileRepo {

  def getMobileType(mobileNumber : String) : Future[Option[MobileProvider]]

}
